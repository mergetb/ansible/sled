# Sled Ansible Role

This role installs and configures [Sled](https://gitlab.com/mergetb/tech/sled) components.


## Variables

| name | required | default | description |
| ---- | ---------| ------- | ----------- |
| sledctl | **no** | | install the sledctl utility  |
| sledd | **no** | yes | install the sledd and nginx services |
| sledd\_image | **no** | mergetb/sledd | container image running sledd |
| nginx\_image | **no** | mergetb/slednginx | container image running nginx for sled |
| docker\_tag | **no** | latest | container tag version |
| mount\_point | **no** | /var/img | localhost's mount point for images |
| sledb\_host | **no** | 172.30.0.1 | location of sledb service (for sledd) |


## Examples

### Sled Controller

```yaml
- include_role:
    name: sled
  vars:
    sledctl: yes
    sled: yes
    sled_tag: dev
```
